/*1 total number fruit on sale*/
db.fruits.aggregate([{ $match: { onSale: true } }, { $count: "fruitsOnSale" }]);

/*2*/
db.fruits.aggregate([
	{ $match: { stock: { $gt: 20 } } },
	{ $count: "enoughStock" },
]);

/*3*/
db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: { _id: null, avg_price: { $avg: "$price" } } },
]);

/*4*/
db.fruits.aggregate([
	{ $match: { onSale: true } },
	{
		$group: {
			_id: null,
			max_price: { $max: "$price" },
		},
	},
]);

/*5*/
db.fruits.aggregate([
	{ $match: { onSale: true } },
	{
		$group: {
			_id: null,
			min_price: { $min: "$price" },
		},
	},
]);
